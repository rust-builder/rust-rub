//! Rust Builder for [Rust](https://github.com/rust-lang/rust).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub rust --help
//! rust - Rust Builder
//!
//! Usage:
//!     rub rust [options] [&lt;lifecycle&gt;...]
//!     rub rust (-h | --help)
//!     rub rust --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;        Set the projects directory.
//!     -b --branch &lt;branch&gt;  Set the build branch. [default: master]
//!     -t --enable-test      Enable tests.
//!     -p --prefix &lt;prefix&gt;  Set the installation prefix.
//!     -u --url &lt;url&gt;        Set the SCM URL.
//!     -h --help             Show this usage.
//!     --target &lt;target&gt;     Target triples to build.
//!     --force-configure     Force the configure lifecycle to run.
//!     --enable-docs         Enable document generation.
//!     --disable-ccache      Disable ccache during build.
//!     --disable-valgrind    Disable valgrind testing.
//!     --version             Show rust-rub version.
//! </pre>
//!
//! # Examples
//! ```rust
//! # extern crate buildable; extern crate rust_rub; fn main() {
//! use buildable::Buildable;
//! use rust_rub::RustRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut rr = RustRub::new();
//! let b = Buildable::new(&mut rr, &vec!["rub".to_string(),
//!                                       "rust".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use scm::git::GitCommand;
use utils::usable_cores;
use utils::empty::to_opt;
use docopt::Docopt;
use std::default::Default;
use std::io::fs;
use std::io::fs::PathExtensions;

static USAGE: &'static str = "rust - Rust Builder

Usage:
    rub rust [options] [<lifecycle>...]
    rub rust (-h | --help)
    rub rust --version

Options:
    -d --dir <dir>        Set the projects directory.
    -b --branch <branch>  Set the build branch. [default: master]
    -t --enable-test      Enable tests.
    -p --prefix <prefix>  Set the installation prefix.
    -u --url <url>        Set the SCM URL.
    -h --help             Show this usage.
    --targets=<targets>   Target triples to build.
    --force-configure     Force the configure lifecycle to run.
    --enable-docs         Enable document generation.
    --disable-ccache      Disable ccache during build.
    --disable-valgrind    Disable valgrind testing.
    --version             Show rust-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_targets: String,
    flag_force_configure: bool,
    flag_enable_docs: bool,
    flag_disable_ccache: bool,
    flag_disable_valgrind: bool,
    flag_version: bool,
    flag_help: bool,
    arg_lifecycle: Vec<String>,
}

#[cfg(target_os = "linux")]
fn os_clean(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("sudo");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["make", "clean"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_clean(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("make");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["clean"]);
    cmd.exec(to_res())
}

#[cfg(target_os = "linux")]
fn os_install(cfg: &BuildConfig) -> Result<u8,u8> {
    let mut jobs = String::from_str("-j");
    jobs.push_str(usable_cores().to_string().as_slice());
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("sudo");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["make", jobs.as_slice(), "install"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_install(cfg: &BuildConfig) -> Result<u8,u8> {
    let mut jobs = String::from_str("-j");
    jobs.push_str(usable_cores().to_string().as_slice());
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("make");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&[jobs.as_slice(), "install"]);
    cmd.exec(to_res())
}

/// rust specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct RustRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    targets: String,
    docs: bool,
    force_config: bool,
    ccache: bool,
    valgrind: bool,
}

impl RustRub {
    /// Create a new default RustRub
    pub fn new() -> RustRub {
        Default::default()
    }
}

impl Buildable for RustRub {
    /// Update the RustRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate rust_rub; fn main() {
    /// use buildable::Buildable;
    /// use rust_rub::RustRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = RustRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "rust".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut RustRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());
        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.targets = dargs.flag_targets;
        self.force_config = dargs.flag_force_configure;
        self.docs = dargs.flag_enable_docs;
        self.ccache = dargs.flag_disable_ccache;
        self.valgrind = dargs.flag_disable_valgrind;
        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("rust");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `RustRub`.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate rust_rub; fn main() {
    /// use buildable::Buildable;
    /// use rust_rub::RustRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = RustRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "rust".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("rust", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for Rust.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for cargo dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `rust` will be cloned from
    /// github automatically.  You can adjust where is is cloned from by using
    /// the `--url` flag at the command line.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "git@github.com:rust-lang/rust.git"
        } else {
            self.url.as_slice()
        };

        let mut res: Result<u8,u8> = if !base.join(cfg.get_project()).exists() {
            GitCommand::new()
                .wd(base.clone())
                .verbose(true)
                .clone(Some(vec!["--recursive", u]), to_res())
        } else {
            Ok(0)
        };

        res = if res.is_ok() {
            GitCommand::new()
                .wd(base.join(cfg.get_project()))
                .verbose(true)
                .update_branch(self.config.get_branch())
        } else {
            res
        };

        res
    }

    /// Run `make clean` in the project directory.
    ///
    /// # Notes
    /// * This will fail if `configure` hasn't been run at least once, because
    /// no `Makefile` will exist.
    fn clean(&self) -> Result<u8,u8> {
        os_clean(&self.config)
    }

    /// Run `configure` in the project directory.
    ///
    /// # Examples
    /// <pre>
    /// rub rust --force-configure
    /// rub rust --prefix=/usr/local --disable-ccache --disable-valgrind
    /// rub rust -b auto
    /// </pre>
    ///
    /// # Notes
    /// * If a `Makefile` doesn't exist or `--force-configure` was supplied then
    /// configure will run.  Otherwise, it will be skipped.
    /// * The default `configure` command is setup as:
    /// ``
    /// configure --prefix=/opt/rust-<branch> --enable-valgrind --enable-ccache
    /// ``
    /// * You can adjust the `prefix` by using the `--prefix <x>` flag or the
    /// `--branch X` flag.
    /// * Use the `--disable-ccache` or `--disable-valgrind` flags to disable
    /// the respective `ccache` and `valgrind` usage.
    /// * If you wish to cross-compile, you can use the `--target` flag to
    /// supply a comma separated list of target triples.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());

        if !wd.join("Makefile").exists() || wd.join("dirty").exists() || self.force_config {
            fs::unlink(&Path::new(wd.join("dirty"))).unwrap_or_else(|why| {
                println!("{}", why);
            });
            let mut cmd = CommandExt::new("configure");
            cmd.wd(&wd);
            cmd.header(true);
            let mut fullp = String::from_str("--prefix=");

            if self.prefix.is_empty() {
                fullp.push_str("/opt/rust-");
                fullp.push_str(self.config.get_branch());
            } else {
                fullp.push_str(self.prefix.as_slice());
            }
            cmd.arg(fullp.as_slice());

            let mut targets = String::from_str("--target=");

            if !self.targets.is_empty() {
                targets.push_str(self.targets.as_slice());
                println!("{}", targets);
                cmd.arg(targets.as_slice());
            }

            if !self.docs {
                cmd.arg("--disable-docs");
            }

            if !self.ccache {
                cmd.arg("--enable-ccache");
            }

            if !self.valgrind {
                cmd.arg("--enable-valgrind");
            }

            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make' in the project directory.
    ///
    /// Runs `make -j<X>` where X is the number of usable cores.
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("make");
        cmd.wd(&wd);
        cmd.arg(jobs.as_slice());
        cmd.header(true);
        cmd.exec(to_res())
    }

    /// Run 'make check' in the project directory..
    fn test(&self) -> Result<u8,u8> {
        if self.config.get_test() {
            let cfg = &self.config;
            let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
            let mut cmd = CommandExt::new("make");
            cmd.wd(&wd);
            cmd.args(&["check"]);
            cmd.header(true);
            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make install' or 'sudo make install' in the project directory.
    ///
    /// Runs `make -j<X> install` where X is the number of usable cores.
    fn install(&self) -> Result<u8,u8> {
        os_install(&self.config)
    }

    /// Not implemented in this crate.
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} rust-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::RustRub;

    fn check_rr(rr: &RustRub) {
        assert_eq!(rr.prefix, "");
        assert_eq!(rr.url, "");
        assert!(rr.targets.is_empty());
        assert!(!rr.force_config);
        assert!(!rr.docs);
        assert!(!rr.ccache);
        assert!(!rr.valgrind);
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let proj = Path::new(env!("HOME").to_string()).join("projects");

        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), proj.as_str().unwrap());
        assert_eq!(bc.get_project(), "rust");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let rr = RustRub::new();
        check_rr(&rr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "rust".to_string(),
                        "--version".to_string()];
        let mut rr = RustRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "rust".to_string(),
                        "-h".to_string()];
        let mut rr = RustRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "rust".to_string()];
        let mut rr = RustRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "rust".to_string(),
                        "scm".to_string()];
        let mut rr = RustRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "rust".to_string(),
                        "all".to_string()];
        let mut rr = RustRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }
}
